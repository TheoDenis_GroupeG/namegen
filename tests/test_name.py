import unittest
from name_generator import *
import dic.universes

class Test_Name_Gen(unittest.TestCase):

    def test_extract_letter(self):
        self.assertEqual({"first" : 'E', "last" : 'S'}, extract({"first" : 2, "last" : 10}, "theo", "denis"))
        self.assertEqual({"first" : 'T', "last" : '2'}, extract({"first" : 0, "last" : 1}, "Test", "1212"))
        self.assertNotEqual({"first" : 'T', "last" : 't'}, extract({"first" : 0, "last" : 0}, "Test", "test"))
        self.assertEqual({"first" : 'C', "last" : 'I'}, extract({"first" : 5, "last" : 0}, "Patrick", "Balkany"))